﻿using System;

namespace WebApiProject.Models
{
    public class Session
    {
        public int Id { get; set; }
        public string SessionString { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsExpire { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

    }
}