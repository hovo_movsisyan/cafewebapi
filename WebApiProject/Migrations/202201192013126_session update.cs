﻿namespace WebApiProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sessionupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sessions", "IsExpire", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sessions", "IsExpire");
        }
    }
}
