﻿namespace WebApiProject.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebApiProject.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApiProject.Data.CafeDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }


        protected override void Seed(Data.CafeDbContext context)
        {
            context.Menus.AddOrUpdate(x => x.Id,
           new Menu() { Id = 1, Name = "Coffee", Image = "https://i.imgur.com/32myl6t.jpg" },
           new Menu() { Id = 2, Name = "Tea", Image = "https://i.imgur.com/BbJvKUl.jpg" },
           new Menu() { Id = 3, Name = "Cold Drinks", Image = "https://i.imgur.com/Rfvwsti.jpg" }
           );

            context.SubMenus.AddOrUpdate(x => x.SubMenuId,
                new SubMenu()
                {
                    SubMenuId = 1,
                    Name = "Cappuccino",
                    Description = "A cappuccino is an espresso - based coffee drink that originated in Italy, and is traditionally prepared with double espresso, and steamed milk foam.",
                    Price = "10 USD",
                    Image = "https://s15.postimg.cc/gs8p61egb/cappuccino.jpg",
                    Menu_Id = 1
                },
                new SubMenu()
                {
                    SubMenuId = 2,
                    Name = "Americano",
                    Description = "Caffè Americano is a type of coffee prepared by diluting an espresso with hot water, giving it a similar strength to, but different flavor from traditionally brewed coffee.",
                    Price = "10 USD",
                    Image = "https://s15.postimg.cc/nvgklnc63/americano.jpg",
                    Menu_Id = 1
                },
                new SubMenu()
                {
                    SubMenuId = 3,
                    Name = "Mocha",
                    Description = "A caffè mocha, also called mocaccino, is a chocolate - flavored variant of a caffè latte.Other commonly used spellings are mochaccino and also mochachino.",
                    Price = "15 USD",
                    Image = "https://s15.postimg.cc/dle5mfh5n/mocha.jpg",
                    Menu_Id = 1
                },
                new SubMenu()
                {
                    SubMenuId = 4,
                    Name = "Green Tea",
                    Description = "Green tea is a type of tea that is made from Camellia sinensis leaves that have not undergone the same withering and oxidation process used to make oolong teas and black teas.",
                    Price = "10 USD",
                    Image = "https://i.imgur.com/LmUJk2V.jpg",
                    Menu_Id = 2
                },
                new SubMenu()
                {
                    SubMenuId = 5,
                    Name = "Black Tea",
                    Description = "Black tea is a type of tea that is more oxidized than oolong, green, and white teas.",
                    Price = "10 USD",
                    Image = "https://i.imgur.com/aNp2kUe.jpg",
                    Menu_Id = 2
                },
                  new SubMenu()
                  {
                      SubMenuId = 6,
                      Name = "Apple",
                      Description = "Juice Apple juice is a fruit juice made by the maceration and pressing of an apple.",
                      Price = "15 USD",
                      Image = "https://i.imgur.com/cKMstjY.jpg",
                      Menu_Id = 3
                  },
                     new SubMenu()
                     {
                         SubMenuId = 7,
                         Name = "Orange Juice",
                         Description = "Orange juice is the liquid extract of the orange tree fruit, produced by squeezing oranges.",
                         Price = "20 USD",
                         Image = "https://i.imgur.com/PC6f3Dc.jpg",
                         Menu_Id = 3
                     }
                );
        }
    }
}
