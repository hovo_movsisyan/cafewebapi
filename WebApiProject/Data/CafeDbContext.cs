﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApiProject.Models;

namespace WebApiProject.Data
{
    public class CafeDbContext : DbContext
    {
        public DbSet<Menu> Menus { get; set; }
        public DbSet<SubMenu> SubMenus { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Session> Sessions { get; set; }

        public CafeDbContext() : base("CafeDB") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<SubMenu>()
                .HasRequired<Menu>(s => s.Menu)
                .WithMany(g => g.SubMenus)
                .HasForeignKey<int>(s => s.Menu_Id);
        }
    }
}