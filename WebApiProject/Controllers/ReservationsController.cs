﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiProject.Data;
using WebApiProject.Models;

namespace WebApiProject.Controllers
{
    [RoutePrefix("api/Reservations")]
    public class ReservationsController : ApiController
    {
        [Route("AddReservation")]
        [HttpPost]
        public IHttpActionResult AddReservation(Reservation reservation)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var expressoDbContext = new CafeDbContext())
            {
                expressoDbContext.Reservations.Add(new Reservation()
                {
                    Id = reservation.Id,
                    Email = reservation.Email,
                    Name = reservation.Name,
                    Phone = reservation.Phone,
                    Date = reservation.Date,
                    Time = reservation.Time,
                    Status = reservation.Status,
                    TotalPeople = reservation.TotalPeople,
                    UserId = reservation.UserId
                });
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }


        [Route("GetOrderById")]
        [HttpPost]
        public IHttpActionResult GetOrderById(Reservation reservation)
        {
            IList<Reservation> reservations = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                reservations = expressoDbContext.Reservations.Where(x => x.UserId == reservation.UserId).ToList();
            }
            if (reservations == null)
                return NotFound();
            return Ok(reservations);
        }

        [Route("RejectionById")]
        [HttpPost]
        public IHttpActionResult RejectionById(Reservation reservationModel)
        {
            Reservation reservation = null;
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var expressoDbContext = new CafeDbContext())
            {
                reservation = expressoDbContext.Reservations.Where(x => x.Id == reservationModel.Id).FirstOrDefault();
                reservation.Status = -1;
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }


        [Route("AproveById")]
        [HttpPost]
        public IHttpActionResult AproveById(Reservation reservationModel)
        {
            Reservation reservation = null;
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var expressoDbContext = new CafeDbContext())
            {
                reservation = expressoDbContext.Reservations.Where(x => x.Id == reservationModel.Id).FirstOrDefault();
                reservation.Status = 1;
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }

        [Route("GetReservations")]
        [HttpGet]
        public IHttpActionResult GetReservations()
        {
            IList<Reservation> reservations = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                reservations = expressoDbContext.Reservations.ToList();
            }
            if (reservations == null)
                return NotFound();
            return Ok(reservations);
        }

        [Route("GetResevationsStatus")]
        [HttpGet]
        public IHttpActionResult GetResevationsStatus()
        {
            IList<Reservation> reservations = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                reservations = expressoDbContext.Reservations.Where(s => s.Status == 0 && s.Date >= DateTime.Now).ToList();
            }
            if (reservations == null)
                return NotFound();
            return Ok(reservations);
        }

        [Route("GetOrderStatus")]
        [HttpPost]
        public IHttpActionResult GetOrderStatus(Reservation reservationModel)
        {
            Reservation reservation = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                reservation = expressoDbContext.Reservations.Where(x => x.Id == reservationModel.Id).FirstOrDefault();
            }
            if (reservation == null)
                return NotFound();
            return Ok(reservation.Status);
        }

        [Route("DeleteReservation")]
        [HttpDelete]
        public IHttpActionResult DeleteReservation(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid menu id");

            using (var expressoDbContext = new CafeDbContext())
            {
                var order = expressoDbContext.Reservations
                    .Where(s => s.Id == id)
                    .FirstOrDefault();

                expressoDbContext.Reservations.Remove(order);
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }
    }
}
