﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiProject.Data;
using WebApiProject.Models;

namespace WebApiProject.Controllers
{
    [RoutePrefix("api/Menus")]
    public class MenusController : ApiController
    {
        readonly CafeDbContext expressoDbContext = new CafeDbContext();



        [Route("GetMenus")]
        [HttpGet]
        public IHttpActionResult GetMenus()
        {
            IList<Menu> menus = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                menus = expressoDbContext.Menus.Include("SubMenus").ToList();
            }
            if (menus == null)
                return NotFound();
            return Ok(menus);
        }

        [Route("GetSubMenus")]
        [HttpGet]
        public IHttpActionResult GetSubMenus()
        {
            IList<SubMenu> subMenus = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                subMenus = expressoDbContext.SubMenus.ToList();
            }
            if (subMenus == null)
                return NotFound();
            return Ok(subMenus);
        }

        [Route("DeleteMenu")]
        [HttpDelete]
        public IHttpActionResult DeleteMenu(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid menu id");

            using (var expressoDbContext = new CafeDbContext())
            {
                var menu = expressoDbContext.Menus
                    .Where(s => s.Id == id)
                    .FirstOrDefault();

                expressoDbContext.Menus.Remove(menu);
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }
        [Route("DeleteSubMenu")]
        [HttpDelete]
        public IHttpActionResult DeleteSubMenu(int id)
        {
            if (id <= 0)
                return BadRequest("Not a valid submenu id");

            using (var expressoDbContext = new CafeDbContext())
            {
                var subMenu = expressoDbContext.SubMenus
                    .Where(s => s.SubMenuId == id)
                    .FirstOrDefault();

                expressoDbContext.SubMenus.Remove(subMenu);
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }

        [Route("AddMenu")]
        [HttpPost]
        public IHttpActionResult AddMenu(Menu menu)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var expressoDbContext = new CafeDbContext())
            {
                expressoDbContext.Menus.Add(new Menu()
                {
                    Id = menu.Id,
                    Name = menu.Name,
                    Image = menu.Image
                });
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }

        [Route("AddSubMenu")]
        [HttpPost]
        public IHttpActionResult AddSubMenu(SubMenu subMenu)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            using (var expressoDbContext = new CafeDbContext())
            {
                expressoDbContext.SubMenus.Add(new SubMenu()
                {
                    SubMenuId = subMenu.SubMenuId,
                    Name = subMenu.Name,
                    Image = subMenu.Image,
                    Description=subMenu.Description,
                    Price=subMenu.Price,
                    Menu_Id=subMenu.Menu_Id
                });
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }
    }
}
