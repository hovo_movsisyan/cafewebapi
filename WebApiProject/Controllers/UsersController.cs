﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiProject.Data;
using WebApiProject.Models;

namespace WebApiProject.Controllers
{
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        [Route("Register")]
        [HttpPost]
        public IHttpActionResult Register(User user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            string session = Guid.NewGuid().ToString();

            using (var expressoDbContext = new CafeDbContext())
            {
                expressoDbContext.Users.Add(new User()
                {
                    UserId = user.UserId,
                    Name = user.Name,
                    Email = user.Email,
                    Password = user.Password,
                    Sessions = new List<Session>
                    {
                        new Session
                        {
                            SessionString = session,
                            IsExpire = false,
                            CreationDate = DateTime.Now,
                            ExpirationDate = DateTime.Now.AddDays(25)
                        }
                    }
                });
                expressoDbContext.SaveChanges();
            }
            return Ok();
        }

        [Route("Login")]
        [HttpPost]
        public IHttpActionResult Login(User client)
        {
            User user = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                user = expressoDbContext.Users
                    .Where(s => s.Email == client.Email && s.Password == client.Password).FirstOrDefault<User>();
            }

            if (user == null)
                return NotFound();

            return Ok(user.UserId);
        }

        [Route("GetUserById")]
        [HttpPost]
        public IHttpActionResult GetUserById(User client)
        {
            UserViewModel user = null;

            using (var expressoDbContext = new CafeDbContext())
            {
                user = expressoDbContext.Users.Include("Reservations")
                    .Where(s => s.UserId == client.UserId)
                    .Select(s => new UserViewModel()
                    {
                        UserId = s.UserId,
                        Name = s.Name,
                        Email = s.Email,
                        Password = s.Password
                    }).FirstOrDefault<UserViewModel>();
            }

            if (user == null)
                return NotFound();

            return Ok(user);
        }
    }
}
