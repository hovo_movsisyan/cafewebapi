﻿namespace WebApiProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubMenus",
                c => new
                    {
                        SubMenuId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Price = c.String(),
                        Image = c.String(),
                        Menu_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SubMenuId)
                .ForeignKey("dbo.Menus", t => t.Menu_Id, cascadeDelete: true)
                .Index(t => t.Menu_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubMenus", "Menu_Id", "dbo.Menus");
            DropIndex("dbo.SubMenus", new[] { "Menu_Id" });
            DropTable("dbo.SubMenus");
            DropTable("dbo.Menus");
        }
    }
}
